#ifndef SHT85_H
#define SHT85_H

#include <Arduino.h>
#include <Wire.h>

#include "crc.h"

const uint8_t SHT85_ADDRESS = 0x44;

enum Sht85Periodic
{
    SHT85_HALFHERTZ_HIGH = 0x2032,
    SHT85_HALFHERTZ_MED = 0x2024,
    SHT85_HALFHERTZ_LOW = 0x202f,
    SHT85_ONEHERTZ_HIGH = 0x2130,
    SHT85_ONEHERTZ_MED = 0x2126,
    SHT85_ONEHERTZ_LOW = 0x212d,
    SHT85_TWOHERTZ_HIGH = 0x2236,
    SHT85_TWOHERTZ_MED = 0x2220,
    SHT85_TWOHERTZ_LOW = 0x222b,
    SHT85_FOURHERTZ_HIGH = 0x2334,
    SHT85_FOURHERTZ_MED = 0x2322,
    SHT85_FOURHERTZ_LOW = 0x2329,
    SHT85_TENHERTZ_HIGH = 0x2737,
    SHT85_TENHERTZ_MED = 0x2721,
    SHT85_TENHERTZ_LOW = 0x272a
};

enum Sht85Repeatability
{
    SHT85_LOW,
    SHT85_MED,
    SHT85_HIGH
};

class Sht85Sensor
{
public:
    Sht85Sensor();

    bool periodicStart(uint16_t cmd);
    bool periodicFetch(uint8_t repeatability = SHT85_HIGH);
    bool periodicStop();
    bool readSingleShot(uint8_t repeatability = SHT85_HIGH);
    bool readStatusRegister();
    uint32_t getSerialNumber();

    bool softReset();
    bool enableHeater();
    bool disableHeater();
    bool clearStatusRegister();

    float getTemperatureC() { return temperatureC; }
    float getTemperatureF() { return (temperatureC * 9 / 5) + 32; }
    float getHumidity() { return humidity; }

    bool isAlertPending() { return status & PENDING_ALERT; }
    bool isHeaterOn() { return status & HEATER_STATUS; }
    bool isHumidityAlerted() { return status & HUMIDITY_ALERT; }
    bool isTemperatureAlerted() { return status & TEMPERATURE_ALERT; }
    bool wasSystemReset() { return status & SYSTEM_RESET; }
    bool wasCommandExecuted() { return status & COMMAND_STATUS; }
    bool wasWriteChecksumValid() { return status & CHECKSUM_STATUS; }

private:
    bool i2cSend(const uint8_t* cmd);
    bool i2cRead(const uint8_t* cmd, uint8_t* data, uint8_t dataLen, uint8_t delay);
    bool isValidPeriodicCommand(uint16_t cmd);

    float convertRawTemperatureToC(uint16_t raw);
    float convertRawHumidity(uint16_t raw);

    uint8_t address;
    uint16_t status;

    float humidity;
    float temperatureC;

    enum DelayMs
    {
        DELAY_RESET = 2,
        DELAY_MEAS_LOW = 2,
        DELAY_MEAS_MED = 7,
        DELAY_MEAS_HIGH = 16,
        DELAY_SERIAL = 1,
        DELAY_PERIODIC_STOP = 1
    };

    enum Command
    {
        CMD_MEAS_LOW = 0x2416,
        CMD_MEAS_MED = 0x240b,
        CMD_MEAS_HIGH = 0x2400,
        CMD_SERIAL_NUM = 0x3682,
        CMD_STATUS = 0xf32d,
        CMD_STATUS_CLEAR = 0x3041,
        CMD_RESET = 0x30a2,
        CMD_HEATER_ENABLE = 0x306d,
        CMD_HEATER_DISABLE = 0x3066,
        CMD_PERIODIC_FETCH = 0xe000,
        CMD_PERIODIC_STOP = 0x3093
    };

    enum DataLen
    {
        DATALEN_CMD = 2,
        DATALEN_MEAS = 6,
        DATALEN_SERIAL = 6,
        DATALEN_STATUS = 3
    };

    enum StatusMask
    {
        PENDING_ALERT = 0x8000,
        HEATER_STATUS = 0x2000,
        HUMIDITY_ALERT = 0x800,
        TEMPERATURE_ALERT = 0x400,
        SYSTEM_RESET = 0x10,
        COMMAND_STATUS = 0x2,
        CHECKSUM_STATUS = 0x1
    };
};

#endif
