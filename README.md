sht85
=====

Sht85 is an [Arduino](https://www.arduino.cc/) library built for reading a [Sensiron SHT85 Temperature/Humidity](https://www.sensirion.com/en/environmental-sensors/humidity-sensors/sht85-pin-type-humidity-sensor-enabling-easy-replaceability/) sensor.  Many of the code concepts were taken from the Sensiron provided [arduino-sht](https://github.com/Sensirion/arduino-sht) library.  The reason for this library was to completely enable all the functionality of the sensor in one library.  Note that this library has only been built to support the SHT85 specifically.

Installation
------------

Download or clone this repository and place it in the `libraries` directory of your Arduino installation.

Usage
-----

Below is a simple example of usage of the sht85 library.  Another example may be found in the [examples](examples) directory.

```cpp
#include <Wire.h>
#include <sht85.h>

Sht85Sensor sht;

void setup()
{
  Wire.begin();
  Serial.begin(9600);
  delay(1000);
}

void loop()
{
  if (sht.readSingleShot())
  {
    Serial.print("\ntempF: ");
    Serial.print(sht.getTemperatureF());
    Serial.print("\nhumidity: ");
    Serial.print(sht.getHumidity());
  }

  delay(1000);
}
```
