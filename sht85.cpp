#include "sht85.h"

Sht85Sensor::Sht85Sensor(): address(SHT85_ADDRESS)
{
}

bool Sht85Sensor::i2cSend(const uint8_t* cmd)
{
    Wire.beginTransmission(address);

    for (int i = 0; i < DATALEN_CMD; ++i)
    {
        if (Wire.write(cmd[i]) != 1)
        {
            return false;
        }
    }

    if (Wire.endTransmission(false) != 0)
    {
        return false;
    }

    return true;
}

bool Sht85Sensor::i2cRead(const uint8_t* cmd, uint8_t* data, uint8_t dataLen, uint8_t delayMs)
{
    i2cSend(cmd);

    delay(delayMs);

    Wire.requestFrom(address, dataLen);

    if (Wire.available() != dataLen)
    {
        return false;
    }

    for (int i = 0; i < dataLen; ++i)
    {
        data[i] = Wire.read();
    }

    return true;
}

float Sht85Sensor::convertRawTemperatureToC(uint16_t raw)
{
    return (175 * (float)raw / (0xffff - 1)) - 45;
}

float Sht85Sensor::convertRawHumidity(uint16_t raw)
{
    return (100 * (float)raw / (0xffff - 1));
}

uint32_t Sht85Sensor::getSerialNumber()
{
    uint8_t data[DATALEN_SERIAL];
    uint8_t cmd[DATALEN_CMD];
    uint32_t serial;

    cmd[0] = CMD_SERIAL_NUM >> 8;
    cmd[1] = CMD_SERIAL_NUM & 0xff;

    if (!i2cRead(cmd, data, DATALEN_SERIAL, DELAY_SERIAL))
    {
        return 0;
    }

    if (crc8(&data[0], 2) != data[2] || crc8(&data[3], 2) != data[5])
    {
        return 0;
    }

    serial = (uint32_t)data[4] + ((uint32_t)data[3] << 8) +
             ((uint32_t)data[1] << 16) + ((uint32_t)data[0] << 24);

    return serial;
}

bool Sht85Sensor::readSingleShot(uint8_t repeatability)
{
    uint8_t data[DATALEN_SERIAL];
    uint8_t cmd[DATALEN_CMD];
    uint8_t delay = DELAY_MEAS_HIGH;
    uint16_t rawHumidity = 0;
    uint16_t rawTemperature = 0;

    cmd[0] = CMD_MEAS_HIGH >> 8;
    cmd[1] = CMD_MEAS_HIGH & 0xff;

    switch (repeatability)
    {
        case SHT85_LOW:
            cmd[1] = CMD_MEAS_LOW & 0xff;
            delay = DELAY_MEAS_LOW;
            break;

        case SHT85_MED:
            cmd[1] = CMD_MEAS_MED & 0xff;
            delay = DELAY_MEAS_MED;
            break;
    }

    if (!i2cRead(cmd, data, DATALEN_MEAS, delay))
    {
        return false;
    }

    if (crc8(&data[0], 2) != data[2] || crc8(&data[3], 2) != data[5])
    {
        return false;
    }

    rawTemperature = (data[0] << 8) + data[1];
    rawHumidity = (data[3] << 8) + data[4];

    temperatureC = convertRawTemperatureToC(rawTemperature);
    humidity = convertRawHumidity(rawHumidity);

    return true;
}

bool Sht85Sensor::readStatusRegister()
{
    uint8_t data[DATALEN_STATUS];
    uint8_t cmd[DATALEN_CMD];

    cmd[0] = CMD_STATUS >> 8;
    cmd[1] = CMD_STATUS & 0xff;

    if (!i2cRead(cmd, data, DATALEN_STATUS, DELAY_SERIAL))
    {
        return false;
    }

    if (crc8(&data[0], 2) != data[2])
    {
        return false;
    }

    status = (data[0] << 8) + data[1];

    return true;
}

bool Sht85Sensor::softReset()
{
    uint8_t cmd[DATALEN_CMD];

    cmd[0] = CMD_RESET >> 8;
    cmd[1] = CMD_RESET & 0xff;

    return i2cSend(cmd);
}

bool Sht85Sensor::enableHeater()
{
    uint8_t cmd[DATALEN_CMD];

    cmd[0] = CMD_HEATER_ENABLE >> 8;
    cmd[1] = CMD_HEATER_ENABLE & 0xff;

    return i2cSend(cmd);
}

bool Sht85Sensor::disableHeater()
{
    uint8_t cmd[DATALEN_CMD];

    cmd[0] = CMD_HEATER_DISABLE >> 8;
    cmd[1] = CMD_HEATER_DISABLE & 0xff;

    return i2cSend(cmd);
}

bool Sht85Sensor::clearStatusRegister()
{
    uint8_t cmd[DATALEN_CMD];

    cmd[0] = CMD_STATUS_CLEAR >> 8;
    cmd[1] = CMD_STATUS_CLEAR & 0xff;

    return i2cSend(cmd);
}

bool Sht85Sensor::periodicStart(uint16_t cmd_in)
{
    if (!isValidPeriodicCommand(cmd_in))
    {
        return false;
    }

    uint8_t cmd[DATALEN_CMD];

    cmd[0] = cmd_in >> 8;
    cmd[1] = cmd_in & 0xff;

    bool sent = i2cSend(cmd);

    if (!sent)
    {
        return false;
    }

    delay(DELAY_PERIODIC_STOP);
    return true;
}

bool Sht85Sensor::periodicFetch(uint8_t repeatability)
{
    uint8_t data[DATALEN_SERIAL];
    uint8_t cmd[DATALEN_CMD];
    uint8_t delay = DELAY_MEAS_HIGH;
    uint16_t rawHumidity = 0;
    uint16_t rawTemperature = 0;

    cmd[0] = CMD_MEAS_HIGH >> 8;
    cmd[1] = CMD_MEAS_HIGH & 0xff;

    switch (repeatability)
    {
        case SHT85_LOW:
            cmd[1] = CMD_MEAS_LOW & 0xff;
            delay = DELAY_MEAS_LOW;
            break;

        case SHT85_MED:
            cmd[1] = CMD_MEAS_MED & 0xff;
            delay = DELAY_MEAS_MED;
            break;
    }

    if (!i2cRead(cmd, data, DATALEN_MEAS, delay))
    {
        return false;
    }

    if (crc8(&data[0], 2) != data[2] || crc8(&data[3], 2) != data[5])
    {
        return false;
    }

    rawTemperature = (data[0] << 8) + data[1];
    rawHumidity = (data[3] << 8) + data[4];

    temperatureC = convertRawTemperatureToC(rawTemperature);
    humidity = convertRawHumidity(rawHumidity);

    return true;
}

bool Sht85Sensor::periodicStop()
{
    uint8_t cmd[DATALEN_CMD];

    cmd[0] = CMD_PERIODIC_STOP >> 8;
    cmd[1] = CMD_PERIODIC_STOP & 0xff;

    bool sent = i2cSend(cmd);

    if (!sent)
    {
        return false;
    }

    delay(DELAY_PERIODIC_STOP);
    return true;
}


bool Sht85Sensor::isValidPeriodicCommand(uint16_t cmd)
{
    switch (cmd)
    {
        case SHT85_HALFHERTZ_HIGH:
        case SHT85_HALFHERTZ_MED:
        case SHT85_HALFHERTZ_LOW:
        case SHT85_ONEHERTZ_HIGH:
        case SHT85_ONEHERTZ_MED:
        case SHT85_ONEHERTZ_LOW:
        case SHT85_TWOHERTZ_HIGH:
        case SHT85_TWOHERTZ_MED:
        case SHT85_TWOHERTZ_LOW:
        case SHT85_FOURHERTZ_HIGH:
        case SHT85_FOURHERTZ_MED:
        case SHT85_FOURHERTZ_LOW:
        case SHT85_TENHERTZ_HIGH:
        case SHT85_TENHERTZ_MED:
        case SHT85_TENHERTZ_LOW:
            return true;

        default:
            return false;
    }
}
