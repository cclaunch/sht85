#include <Wire.h>
#include <sht85.h>

// declare our sensor
Sht85Sensor sht;

void setup()
{
    // set up the i2c interface
    Wire.begin();

    // start up our serial and let it settle
    Serial.begin(9600);
    delay(1000);
}

void loop()
{
    // read a single pair of temperature/humidity
    if (sht.readSingleShot())
    {
        Serial.print("\ntempF: ");
        Serial.print(sht.getTemperatureF());
        Serial.print("\nhumidity: ");
        Serial.print(sht.getHumidity());
    }

    delay(1000);
}
