#include <Wire.h>
#include <sht85.h>

// declare our sensor
Sht85Sensor sht;

void setup()
{
    // set up the i2c interface
    Wire.begin();

    // start up our serial and let it settle
    Serial.begin(9600);
    delay(1000);

    // print out the sensor's serial number
    Serial.print("serial: ");
    Serial.print(sht.getSerialNumber());

    // start a periodic read of 1 Hz on high repeatability
    sht.periodicStart(SHT85_ONEHERTZ_HIGH);
}

void loop()
{
    // fetch the next pair of temperature/humidity
    if (sht.periodicFetch())
    {
        Serial.print("\ntempF: ");
        Serial.print(sht.getTemperatureF());
        Serial.print("\nhumidity: ");
        Serial.print(sht.getHumidity());
    }

    delay(1000);
}
